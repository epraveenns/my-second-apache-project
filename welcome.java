// Import required java libraries
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

// Extend HttpServlet class
public class welcome extends HttpServlet {
 


  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
            throws ServletException, IOException
  {
      
	  if (request.getParameter("first_name")!=null)
      {
    	  JavaForm a = new JavaForm();
    	  a.doGet(request, response);
      }
      
      else if (request.getParameter("Search_button")!=null)
      {
    	  EmpSearch b = new EmpSearch();
    	  b.doGet(request, response);
      }
      
      else
      {
    	  EmpView c = new EmpView();
    	  c.doGet(request, response);
      }
  }
  
  public void destroy()
  {
      // do nothing.
  }
}